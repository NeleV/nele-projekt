package ee.valiit.chat;

import java.util.ArrayList;

public class ChatRoom {
    public String room;           //esimene väli (eht muutuja)    String room
    public ArrayList<ChatMessage> pictures = new ArrayList(); //Loon uue arraylisti kuhu läheb useri pilt nimega pictures
   public ArrayList<ChatMessage>messages = new ArrayList(); //loome uue arraylisti kuhu lähevad chati sõnumid, nimega Messages (front endi jaoks)

    //KONSTRUKTOR
    public ChatRoom(String room) {
        this.room=room;  //this viitab sellele siin
        //Loome muutuja mis on arraylist tüüpi
         //messages.add(new ChatMessage("Juku"));    //Lisame näidis messagei

    }

    public void addMessage(ChatMessage msg) { //Uus meetod addMessage ja siin lisame sõnumi messagite alla!
        messages.add(msg);

    }
    public void addPicture(ChatMessage pic) {
        pictures.add(pic);
    }

}