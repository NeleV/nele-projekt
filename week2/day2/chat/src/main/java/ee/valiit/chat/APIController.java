package ee.valiit.chat;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/* Ülesanne:
1. peale useri ja message lisa ka profiilipildi URL, mis salvestub serverisse ja ka kuvatakse HTMLis välja.
Kasutaja saab kolmanda välja, kuhu see URL panna
2. Loo uus chat room lisaks "Generalile"
 */

@RestController //teeb sellest kontrolleri
@CrossOrigin        //Lubab kõik päringud
public class APIController { //Klass
   // ChatRoom general = new ChatRoom("general"); //Loome uue ChatRoomi mille nimi on general (objekt) -VÕTAME ÄRA, pole vaja rohkem sest tekistasime HashMapi

    HashMap<String, ChatRoom> rooms = new HashMap(); //Tekitame uue HashMapi

    //Konstruktor on lihstsalt meetod mis käivitub automaatselt - sarnaneb meetodiga -
    //KONSTRUKTORIL ON SAMA NIMI MIS KLASSIL!

    public APIController () { //Kontsruktori sisse kirjutame meie chati toad
        rooms.put("general", new ChatRoom ("general"));         //Pöördume romsi poole ja teda käseme
        rooms.put("eriline", new ChatRoom ("eriline"));            //nüüd meil 2 tuba olemeas
        rooms.put("materjalid", new ChatRoom ("materjalid"));       //teeme veel ühe toa juurde
    }




    @GetMapping ("/chat/{room}")       //Fromt end teeb päringu sellel aadressilm ning sellel adrel tagastatakse ChatRoom jutuTuba
    ChatRoom jutuTuba (@PathVariable String room) { //Hakkame chatroomi looma //Meetod (sisendparameeter)
        return rooms.get(room); //Tahame, et tagastaks stringi mitte muutuja ja tahame et ta võtaks midagi vastu - ehk .get
    }
    @PostMapping("/chat/{room}/new-message") //
    void newPicture(@RequestBody ChatMessage msg, @PathVariable String room) {
        rooms.get(room).addMessage(msg); //Panene messegi sellesse general chatroomi, ehk määrame et see läheb ruumi general.add(lisa) Message

    }

   /*  ChatRoom eriline = new ChatRoom("eriline"); //UUs chatroom nimega eriline

    @GetMapping ("/chat/eriline") //Front endi päringu jaoks
    ChatRoom jutukas () {
        return eriline;
    }


   @PostMapping("/chat/eriline/new-message")
    void uusSonum (@RequestBody ChatMessage msg) {
        eriline.addMessage(msg);
   } */

}
