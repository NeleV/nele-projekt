package ee.valiit.chat;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.awt.*;
import java.util.Date;

public class ChatMessage {
                                //3 omadust, message ise, kes saatis (user) ja date
                             //peame looma konstructori, et see ära defineerida. MIKS? Et lisada user message külge!

    private String user;
    private String message; //Alt enter sõna peal - create getter for message
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")//uus annotation ja alt+enter !!PEab olema Date peal, muidu ei tööta!
    private Date date;
    private String picture;


    public ChatMessage() {

    }

    public ChatMessage(String picture, String user, String message) { //Loome uue message ja loome uue date-i
        this.user = user; //Võtame sellest stringist väja
        this.message = message;
        this.date = new Date(); //Suure tähega, nagu objektid ja klassid!
        this.picture = picture;

    }


    public String getUser() {  //Getter meetod
        return user; //tagastab user väärtuse
    }

    public String getMessage() {

        return message; //tagastab message väärtuse
    }

    public Date getDate() {

        return date;    //tagastab date väärtuse
    }

    public String getPicture() {

        return picture;         //tagastab Pic väärtuse
    }
}
