package ee.valiit.chat; //see klass on ainult selleks et teaks kuidas sõnumit vastu võtta JSONiga konventeerida

public class ChatMessage {  //Klass
    private int ID;
    private String username;
    private String room;
    private String message;
    private String picture; //pilt 5. defineerime muutuja nimega picture -järg chatapplicatioc


    public ChatMessage() {}  //Tühi konstruktor, et stringi postmäpping töötaks!

    public ChatMessage(String username, String message, String picture) { //KONSTRUKTOR Chatmessage (tuleb apicontrolerist) //Pilt 9: lisame STring pildi siia!
        this.username = username;
        this.message = message;
        this.picture = picture;             //pilt 10: lisame et võtaks pildi - järg kontroller, siis jõuab info kõik front endi. sisestamine ja väljastamine valmis - järg et kuvada. -->script

    }


    public int getID() {
        return ID;
    }

    public String getUsername() {
        return Security.xssFix(username);
    }

    public String getRoom() {
        return room;
    }

    public String getMessage() {
        return Security.xssFix(message); //Ükskõik kus kasutan shat messaget, on see automaagiliselt kaitstud
    }

    public String getPicture() { //pilt 4: teeme meetodi, et tagastaks pildi
        return Security.xssFix(picture);
    }
}
