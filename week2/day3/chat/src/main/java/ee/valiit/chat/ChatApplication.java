package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class ChatApplication implements CommandLineRunner { //Käivitab commandline-i (CommandLineRUnner)

	public static void main(String[] args) {

		SpringApplication.run(ChatApplication.class, args);
	}

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Start");
		jdbcTemplate.execute("DROP TABLE IF EXISTS messages");      //tabelid, khu saklvestame username, message, pildi
		jdbcTemplate.execute("CREATE TABLE messages (id SERIAL, username text, message text, picture TEXT, room text)"); //Pilt 6 - lisasime - järg
	}
}

/* KODUNE ÜL:
1. Profiili pilt uuesti korda teha
2. Jututoa vahetamine jälle tööle
 */
