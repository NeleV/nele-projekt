package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin
public class APIController { //Klass

    @Autowired                       //Käsk: ühendab ära jdbc klassi - ( ühenda Jdcb dependeci ühendus andmebaasiga)
            JdbcTemplate jdbcTemplate;       //Esimene klass, teine muutuja!!


    @GetMapping("/chat/{room}") //SELECT * FROM messages on käsk mis tagastab meile ridu andmebaasist   //Front end teeb päringu sellel aadressilm ning sellel adrel tagastatakse ChatRoom jutuTuba
    ArrayList<ChatMessage> chat(@PathVariable String room) {
        try {         //TESTIME!

            String sqlKask = "SELECT * FROM messages WHERE  room='"+room+"'";  //SQL käsk mis võrdub = käsk! mis küsib andmeid baasist
            ArrayList<ChatMessage> messages = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {   //(castime ArrayListiks)JSON tahab anmepäringut migis kindlas formaadis - seekord paneme ArrayListi. paneme jdbc muutuja, tühik eraldab käske, Query pärib
                String username = (resultSet.getString("username"));
                String message = (resultSet.getString("message"));
                String picture = (resultSet.getString("picture")); //Pilt 8: võtab vastu?
                return new ChatMessage(username, message, picture);  //loome uue objeti Chatmessage ja teeme talle uue konstruktori mis võtab Chatmessage väärtused külge
            });
            return messages;
        } catch (DataAccessException err) { //TEST  DataAccesExept - java Quesry septsiifiline error!    ERR-muutuja nimi
            System.out.println("TABLE WAS NOT READY!");
            return new ArrayList();         //(TEST -try catch
        }
    }


    @PostMapping("/chat/{room}/new-message")
    void newMessage(@RequestBody ChatMessage msg, @PathVariable String room) {

          String sqlKask = "INSERT INTO messages (username, message, picture, room) VALUES ('" +
                  msg.getUsername() + "', '" +
                  msg.getMessage() + "', '" +
                  msg.getPicture() + "', '" +                  //pilt 3: sisesta andmebaasi uus väärtus, et displayks pilti! - järgmine peame tegema tabelis koha pildi jaoks ja et ta oskaks pilti vastu võtta - järg chatessage
                  room + "')";                                //roomi lõpp - ) lõpetab rea.  Teeme sisestuse, et sõnumid hakkavad messages tabelisse jooksma
        jdbcTemplate.execute(sqlKask);


    }


}
