import java.util.*;

/**
 * Generates a prison to be used for testing
 */
public class PrisonGenerator {

    private Random rand = new Random(2000L);
    private Person supervisor = new Person(PrisonTest.SUPERVISOR_FIRST_NAME, PrisonTest.SUPERVISOR_LAST_NAME);
    private int id = 0;
    private List<PrisonRoom> rooms = new ArrayList<>();

    List<PrisonRoom> generateTestPrison(Person... prisoners) {
        PrisonRoom room = createRoom();

        Map<Person, PrisonRoom> cells = new HashMap<>();
        for (Person prisoner : prisoners) {
            HashSet<Person> allowedPersons = new HashSet<>();
            allowedPersons.add(supervisor);
            allowedPersons.add(prisoner);

            PrisonRoom neighbour = rooms.get(rand.nextInt(rooms.size()));

            PrisonRoom cell = new PrisonRoom(id++, allowedPersons);
            cell.getNeighbours().add(neighbour);
            neighbour.getNeighbours().add(cell);
            rooms.add(cell);
            cells.put(prisoner, cell);
        }

        PrisonRoom.setCells(cells);
        return rooms;
    }

    private PrisonRoom createRoom() {
        HashSet<Person> allowedPersons = new HashSet<>();
        allowedPersons.add(supervisor);

        PrisonRoom room = new PrisonRoom(id++, allowedPersons);
        rooms.add(room);

        int numberOfConnectedRooms = rand.nextInt(4);
        for (int i = 0; i < numberOfConnectedRooms; i++) {
            boolean isConnectedToNew = rand.nextBoolean();
            PrisonRoom neighbour;
            if (isConnectedToNew) {
                neighbour = createRoom();
            } else {
                neighbour = rooms.get(rand.nextInt(rooms.size()));
            }
            room.getNeighbours().add(neighbour);
            neighbour.getNeighbours().add(room);
        }

        return room;
    }
}

