import java.util.ArrayList;

//ül 5: https://gitlab.com/KristerV/valiit-kursus/blob/master/week2/day1/U%CC%88lesanded%205%20-%20Object%20Oriented%20Programming%20basics.pdf
public class Main {  //main klass Main
    public static void main(String[] args) {        //Main meetod

        ArrayList<Sportlane> sportlased = new ArrayList<>(); //Loon uue arraylisti, kuhu paneme kõik sportlased, et saaks nad kergemini kõik välja printida
        sportlased.add(new Skydiver("Kukk")); //Lisame arraylisti Skydiverid
        sportlased.add(new Skydiver( "Kana")); //Lisame arraylisti Skydiverid
        sportlased.add(new Skydiver( "Hani")); //Lisame arraylisti Skydiverid
        sportlased.add(new Runner("Tiiger")); //Lisame arraylisti Runner()id
        sportlased.add(new Runner("Puuma")); //Lisame arraylisti Runner()id
        sportlased.add(new Runner("Gepard")); //Lisame arraylisti Skydiverid

        Skydiver langevarjur1 = new Skydiver("Kukk"); //UUs langevarjur1
        Skydiver langevarjur2 = new Skydiver("Kana"); //UUs langevarjur2
        Skydiver langevarjur3 = new Skydiver("Hani"); //UUs langevarjur3
        Runner jooksja1 = new Runner("Tiiger"); //Uus jooksja
        Runner jooksja2 = new Runner("Puuma");
        Runner jooksja3 = new Runner("Gepard");



        //langevarjur1.getClass().getDeclaredFields();

        for (Sportlane sportlane:sportlased){   //Foreach tsükkel, võtab Sportlaste klassist, uus muutuja sportlane kuhu sisse salvestame kõik sportlased
            System.out.println(sportlane.eesnimi);
            System.out.println(sportlane.perenimi);
            System.out.println(sportlane.vanus);
            System.out.println(sportlane.sugu);
            System.out.println(sportlane.pikkus);
            System.out.println(sportlane.kaal);

            //Käivitan sportlaste perform meetodid

        }






    }



}
