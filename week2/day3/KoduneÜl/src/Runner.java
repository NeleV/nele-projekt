public class Runner extends Sportlane {
    public Runner(String perenimi) {
        super(perenimi);
    }

    @Override
    public void perform() { //See pergorm on ainult selles Runner klassis, kui sportlases käivitada perform, siis runner ja skydiver teevad mõlemad OMA performi!
        System.out.println(perenimi + ": jooksen ja higistan");
    }
}
