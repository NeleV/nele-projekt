public abstract class Sportlane {        //abstraktne klass sportlane
    String eesnimi;
    String perenimi;
    int vanus;
    String sugu;
    double pikkus;
    double kaal;

    public abstract void perform();  //uus meetod, mille kaudu teeb sportlane oma spordiala. Abstrakse klassi puhul on semikoolon! See määrab ära, et kõikidel meetoditel on tegevus nimega perform, kuid tegevus erinev

    public Sportlane(String perenimi){ //KONSTRUKTOR
        this.perenimi=perenimi;
    }


    }

