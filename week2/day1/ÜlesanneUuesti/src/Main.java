import java.math.BigInteger;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {    //Main meetod!! - siia sisse ei defineeri uut meetodit!

        System.out.println("Hello, World!"); //hetkel prindib välja oma main meetodis hello world ja paneb programmi kinni. Siia lisame teised meetodid, mida tahame samuti käivitada
        System.out.println(test(8));  //Kutsume välja Meetodi test ja määrame selle väärtuse - kirjuta sisse AINULT! väärtus, mille määrad!
        System.out.println(test(7));
        System.out.println(test(32));
        test2("voti", "auto");        //Kutsume välja ÜL2, meetodi nimi ja sulgudes (väärtus, mitte väärtuse nimi vaid selle!! väärtus!!)

        System.out.println(addVat(12.0)); //kutsun välja addVati ja määran muutuja väärtuse
        int c = 5;
        int d = 6;
        System.out.println(Meetod(c, d));   //Õieti tehtud, aga massiivi ei ole
        System.out.println(deriveGender("48810260330")); //Kutsun välja meetodi deriveGender, lisan isikukoodile string!! väärtuse ehk "väärtus"!
        String gender = deriveGender("48810260330"); //
        System.out.println(gender); //Kutsun välja main() meetodis ja talletan tulemuse lokaalses main() meetodi muutujas nimega gender.
        // Prindin välja muutuja väärtuse standardväljundisse
        printHello();

        int aastaStr = deriveBirthYear("48810260330"); //Määrame uue muutuja, selles kutsume meetodi välja ja määrame muutuja väärtuse isikukoodile
        System.out.println("Synniaasta on: 19" + aastaStr);

        System.out.println(Main.deriveBirthTime("48810260330"));

        //ÜL 8:
        BigInteger isikukoodNr = new BigInteger("48810260330");
        boolean nimi = validatePersonalCode(isikukoodNr);

        System.out.println(nimi); //Prindime välja


    }
    //1. Defineerime uue meetodi main meetodi kõrvale

    public static boolean test(int taisarv) { //UUS MEETOD (test) DEFINEERITUD, tahame boolean tüüpi väärtust. (Sisendparameeter = määrame mis tüüpi muutuja-antud juhul int ja muutuja nimi) Määrasime sisendparameetri in, ja muutuja nimeks taisarv
        if (taisarv > 0) {       //loome/definreerime meetodi toimimise ilma seda kasutamata/ilma konkreetsete väärtusteta. Ehk kui taisarv on suurem kui 0
            return true;        //Kui konditsioon on õige - tagasta true
        } else {                //Kui konditsioon ei ole õige,
            return false;          // Tagasta false
        }

    }

    //ÜL2: Defineeri meetod nimega test2, milles on kaks String-tüüpi sisendparameetrit ja mis ei tagasta mitte midagi.
    // Kutsu välja Main meetodis
    public static void test2(String a, String b) {  //paneme meetodisse sisse void, kuna ei pea tagastama midagi

    }
    //ÜL3: Defineeri meetod nimega addVat, mis võtab sisendparameetrina ühe double-tüüpi muutuja ja tagastab double-tüüpi muutuja.
    //Kutsu see meetod main() meetodist välja

    public static double addVat(double muutuja) {
        muutuja = muutuja * 1.2;
        return muutuja;
    }

    //ÜL4: Defineeri meetod, mis võtab sisendparameetritena kaks täisarvu ja ühe tõeväärtuse
    // ning tagastab täisarvude massiivi. Nimi mõtle ise välja.
    //Mutsu meetod mainist välja

    public static int[] Meetod(int a, int b) { //Staatiline massiiv meetod [] nimega Meetod, määrame Meetodi parameetrid
        int i = 8; //määrame, et int i on 8
        int[] mas1 = new int[i];      //määrame massiivi nimega mas1, mis on 8 ühiku pikkune
        return mas1;

    }
    //ÜL5: Defineeri meetod deriveGender, mis võtab sisendparameetrina vastu Eesti residendi isikukoodi tekstikujul ja
    // tagastab kõnealuse isikukoodi omaniku
    //soo tekstilisel kujul (kas "M" või "F"
    //Kutsu välja main() meetodist välja ja talleta tulemus lokaalses main() meetodi muutujas nimega gender.Prindi välja
    //muutuja väärtus standardväljundisse

    public static String deriveGender(String isikukood) { //Loome String meetodi nimega deriveGender ja stringi nimi on isikukood
        if (isikukood.startsWith("3") && isikukood.startsWith("5")) {  //Tagastab booleani ja saab olla stringi sees. Kutsu mõlemad välja täielikult!
            return "M";
        } else {
            return "F";
        }

    }
    //ÜL6. Defineeri meetod printHello, mis ei võta vastu ühtki sisendparameetrit
    //Ning mis ei tagasta ühtki väärtust, aga prindib standardväljundisse "Tere".
    //Kutsu välja main meetodist

    public static void printHello() {
        System.out.println("Tere");
    }


    //ÜL7: Defineeri meetod deriveBirthYear, mis võtab sisendparameetrina
    //vastu Eesti residendi isikukoodi tekstikujul ja tagastab kõnealuse isikukoodi omaniku
    //Sünniaasta täisarvulisel kujul.
    //Kutsu see meetod main() meetodist välja ja prindi tulemus standardväljundisse

    public static int deriveBirthYear(String isikukood) { //Loon uue String meetodi deriveBirthYear (parameetriks String isikukood)
        //Võtan Stringist "isikukood" substiring-iga stringist välja erinevaid indexi väärtusi (1, 3) määrab,
        // et võtaks indexid nr 1 ja 2.

        int aastaStr = Integer.parseInt(isikukood.substring(1, 3)); //muudan Stringi integeriks (integer.parseInt)
        return aastaStr; //Miks 1,3 mitte 1,2 - sest alustan 1. indexis kuni!!(mitte kaasa arvatud) 3-index
    }

    //Teine lahenduse variant

    public static int deriveBirthTime(String isikutunnus) {  //Loon  int meetodi deriveBirthTime (String

        String b = isikutunnus.substring(0, 1); //Loon String b = isikutunnusest võtan välja indexid 1-3 (mitte k.a.) numbrid
        int c = Integer.parseInt(b);        //Loon int c muutuja mis konventeerib integeri Stringiks
        int period = ((c - 1) / 2 * 100 + 1800); //int period (uus muutuja) = c(ehk 4 -1) / 2 * 100 + 1800)

        String yearString = isikutunnus.substring(1, 3);

        int year = Integer.parseInt(yearString);
        year += period;
        return year;
    }

    //ÜL8: Defineeri meetod validatePersonalCode, mis võtab sisendparameetrina vastu
    //Eesti residendi isikukoodi BigInteger kujul ja tagastab tõeväärtuse
    //vastavalt sellele, kas kõnealuse isikukoodi kontrollnumber on
    //korrektne või mitte.
    //Isikukoodi kontrollinumbri arvutamine on kirjeldatud siin: wikipedia.org/wiki/Isikukood

    public static boolean validatePersonalCode(BigInteger isikukood) { //Uus meetod boolean tüüpi, sisend on BigInteger
        String iskikukoodStr = isikukood.toString(); //Teiseldan isikukoodi stringiks, et saaksin sealt üksikuid numbreid välja võtta
        int[] astmeKorrutis = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1}; //loome massivi ja täidame numbritega, millega korrutada (nii saab ka!)
       /*  for (char taht: iskikukoodStr.toCharArray()) { //foreach tsükkel, ette paneme muutuja (char -et täht) ja tükeldame
            int number = Integer.parseInt("" + taht); //määrame int muutuja number (teiseldame char muutuja taht ümber intiks (parseint võtab vastu muutuja nimega s:) */
        int summa = 0;//loome uue muutuja, kuhu sisse hakkame tulemusi salvestama. Algväärtus on 0!
        for (int jarg = 0; jarg < astmeKorrutis.length; jarg++) { //Kordab niikaua kuni meil jätkub numbreid
            String taht = iskikukoodStr.substring(jarg, jarg + 1); //Loome uue String muutuja taht. Tahame võtta isikukoodi stringist
            // välja ühe tähe. !!Kaks väärtust - üks (jarg, -algab ja jarg+1 lõppeb) kust alustame välja lõikamist, ja kust lõpetaksime.
            int number = Integer.parseInt(taht); //uus int muutuja nimega number, muudame taht muutuja Intiks
            summa = summa + astmeKorrutis[jarg] * number; //tahame korrutada massiivi null positsioonil olevat elementi. üks korrutist
            // massiivist astmekorrutis, teine isikukoodist (int number). Tahame salvestada vastuse uude muutujasse. Selleks et mitte
            // asendada väärtust vaid liita juurde, paneme summa + tehing
        } //peale tsükklit
        int kontrollNumber = summa % 11; //summa moodul numbrite arvugaisikukoodis, et saada jääk
        if (kontrollNumber == 10) {
           throw new Error("Liiga keeruline");
        }


        int isikukoodInt = Integer.parseInt(iskikukoodStr.substring(10, 11));


        if ((isikukoodInt == kontrollNumber)) { //Kas isikukoodi jääk on sama mis kontrollnumber? - Castime inti char-iks
            return true;//Tagastame
        } else {            //Kui kontrollnumberi jääk on 10
            return false;
        }

        //edasi on vaja veel kontrollnumbri jagamine teha

    }


}

