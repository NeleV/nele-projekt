import java.util.Arrays;

public class Main {
//ÜL 1
    public static void main(String[] args) {
        double pi = 3.14159265358;
        double a = 2.0;
        double b = pi * a;
        System.out.println(b);

        System.out.println(returnIfTrue());

        int tagastus = numbriMassiiv("420");
        System.out.println(tagastus);




        byte saj = sajand(1996);
        System.out.println(saj + ". sajand");

       Country eesti = new Country(13000000, 1300000, "Eesti", "Eesti", "Eesti,vene, inglise");
        eesti.getPopulation();
        eesti.getName();

        System.out.println(eesti);

    }

//ÜL 2
    public static boolean returnIfTrue () {
        int a = 7;
        int b = 8;
        if (a == b) {
            return true;
        } else {
            return false;
        }

    }
//ÜL 3
    public static int numbriMassiiv(String fourTwenty) {
        int tagastus = Integer.parseInt(fourTwenty.substring(0, 3));
        return tagastus;

    }
//ÜL 4
    public static byte sajand(int i) {
        byte saj = (byte) i;
        if (saj > 1900 && saj < 1999 ){
            saj = 20;
        } else if (saj > 1 && saj < 100) {
            saj = 1;
        } else if (saj >= 100 && saj < 200){
            saj = 2;
        } else if (saj >= 500 && saj < 600) {
            saj = 6;
        } else if (saj >= 1600 && saj < 1700){
            saj = 17;
        } else if (saj >= 1800 && saj < 1900){
            saj = 19 * 4;
        } else if (saj >= 1900 && saj < 2000) {
            saj = 20;
        } else if (saj >= 2000 && saj < 2100) {
            saj = 21;
        }
        return saj;
    }

}
/* Ülesanne 5:
Defineeri klass Country, millel oleksid meetodid getPopulation, setPopulation, getName, setName ja list riigis enim
kõneldavate keeltega.

Override’i selle klassi toString() meetod nii, et selle meetodi väljakutsumine tagastaks teksti, mis sisaldaks endas
kõiki parameetreid väljaprindituna.

Tekita antud klassist üks objekt ühe vabalt valitud riigi andmetega ja prindi selle riigi info välja .toString() meetodi
abil. */





