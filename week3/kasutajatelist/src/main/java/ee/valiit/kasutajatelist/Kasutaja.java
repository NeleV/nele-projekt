package ee.valiit.kasutajatelist;

public class Kasutaja {         //anname klassile samad väärtused, mida konventeerime
    private String nimi;        //vali muutuja ja vali alt enteriga getter
    private int vanus;

    public Kasutaja() {

    }

    public String getNimi() {
        return nimi;
    }

    public int getVanus() {
        return vanus;
    }
}
