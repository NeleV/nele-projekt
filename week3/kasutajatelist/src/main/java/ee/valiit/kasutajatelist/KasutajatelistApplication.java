package ee.valiit.kasutajatelist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.concurrent.ThreadPoolExecutor;

@SpringBootApplication
public class KasutajatelistApplication implements CommandLineRunner {

	@Autowired 			//Peab olema!!!
	private JdbcTemplate jbdcTemplate;        //CommandLineRunner, (+ implement methods) meetodisse, et mainis ei saa tabelit luua, selleks siin

	public static void main(String[] args) {
		SpringApplication.run(KasutajatelistApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String sqlKask = "DROP TABLE IF EXISTS kasutajad;" 			//Loome sqlKask stringi milles loome tabeli!!
					+ "CREATE TABLE kasutajad (id SERIAL, nimi TEXT, vanus SMALLINT)";			//Tahab kohe muutuja tüüpi (SQL) - siin lõime ainult taabeli, soovi korral saab andmeid lisada ka siin aga antud juhul me ei tee seda
		jbdcTemplate.execute(sqlKask); 									//jcdb all on meetod execute mida käivitame -punane-vali FIELD! - see paneb SQL Käsu käima!
		System.out.println("Kasutaja tabel loodud.");


	}
}
