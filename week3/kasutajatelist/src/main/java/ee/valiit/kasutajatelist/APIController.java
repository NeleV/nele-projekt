package ee.valiit.kasutajatelist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin                                              //Crossorigin lahndab kõik Restcontrolleriga seotud probleemid ära kuid pole turvaline!!
@RestController                                          //Selleks, et see klass saaks aru, et see moodul on talle mõeldud?

public class APIController {
    @Autowired                                             //seda on vaja JDBC jaoks!!!

    private JdbcTemplate jdbcTemplate;


    @PostMapping ("/list")                                                  //teeme postmappingu, et võtta see JavaScriptist vastu. Nimi peab olema unikaalne. VÕTAB VASTU LIST PATHI!!
    public void handleNewListItem (@RequestBody Kasutaja kasutaja) {            //meetod, määra nimi (vahet pole mis nimi). Tahame, et meetod võtaks vastu klassi, mille kaudu ta konverteerimist teeb, ehk loome klassi! @RequestBody selleks, et ta teaks, et peab seda klassi vastu võtma!
        System.out.println("handleNewListItem");
        System.out.println("Kasutaja nimi: " + kasutaja.getNimi());
        String sqlKask = "INSERT INTO kasutajad (nimi, vanus) VALUES ('"        //Paneme sqlKask sisse SQLi syntaxiga käsu!!! Sisesta tabelisse tulpadesse nimi, vanus saadud kasutaja nime ja vanuse
                + kasutaja.getNimi() + "', " + kasutaja.getVanus() + ");";
        jdbcTemplate.execute(sqlKask);                                          //JDBC objekt. meetod (nimega excecute) (mida excecutime ehk sqlKask) alguses jdbc punanae - create field!
        System.out.println("Sisestamine õnnestus!");
    }
}
