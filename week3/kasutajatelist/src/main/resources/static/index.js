//2. samm - (dokument-kogu HTMLi leht)(form, kui ainult üks form. Kui formi id siis trellid ette!
var url = "http://localhost:8080/list"                          //defineerime muutuja url, ehk anname enda aadressi (võiks midagi seal olla API puhul)
document.querySelector("#form1").onsubmit = function(e) {       //= Lisame uue definitsiooni  mille me ise määrame. (e) - on
    console.log("onsubmit toimib")
    e.preventDefault()                                          //Siis ta ei refreshi kohe automaatselt!
    var nimi = document.querySelector("#nimi").value            //Kuna need hetkel tühjad, siis määrame väärtused
    var vanus = document.querySelector("#vanus").value           //Kuna need hetkel tühjad, siis määrame väärtused
    fetch(url, {                                                //See on post meetod kui me tahame infot kaasa anda
        method: "POST",                                        //Ütleme, et meetodi tüüp on POST
        body: JSON.stringify({nimi, vanus}),                     //Body on see kust me infot kaasa anname!
        headers: {
                   'Accept': 'application/json',
            	    'Content-Type': 'application/json'
        }
    })                                                             //Järg APIController!
}