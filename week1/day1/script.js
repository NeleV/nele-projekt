console.log("Hello World!")
// muuda dokumendi body-s paragraafi//
document.body.innerHTML += "<p>tere!</p>"
//muuda mingit sektsiooni html tekstis nt ol li //
document.querySelector("ol li").innerHTML ="Muudetud!"
//pane "sisesta oma nimi" andmeid koguma//
document.querySelector("form").onsubmit = function(e) {
	//takista selle saatmist e.prevent//
	e.preventDefault()
	var nameInput = document.querySelector("#nimi")
	//console.log("SUBMIT")//
	//ctrl shift i - weebilehe html-css-javascript//
	//console.log(nameInput.value)//
	 // nameInput.value == document.form.output//
	 if(nameInput.value == "Siim") {
	 	document.body.innerHTML += "<p>Tere, "+ nameInput.value + "</p>"
	 } else {
	 	document.body.innerHTML += "<p>Kes sa oled üldse, "+ nameInput.value + "</p>"
	 }
	 
}