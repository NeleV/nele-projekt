import java.util.ArrayList;

public class Suusataja {
    int stardiNr;       //Muutujad
    double kiirus;
    double labitudDistants;
    double dopinguKordaja;
    boolean katkestus;

    public Suusataja(int i) { //konstruktor;
        stardiNr = i; //salvestab muutuja i enda külge
        dopinguKordaja = 1;  //Default dopingu kordaja on üks, ehk kehtib kõigile suusatajatele ja ei muuda midagi
        if (stardiNr == 5) { //kui stardinumbrei on!!! 5, siis dopingukordaha on 1.2 või nt 10
            dopinguKordaja =10; //Dopinguga suusataja liigub 10 korda rohkem kui talle antud random ühe ajaühiku jooksul läbitud distants
        }
        kiirus = Math.random()*20 * dopinguKordaja + 1; //20 km/h - ehk kui pika distantsi iga suusataja ühe sekundiga läbib
        labitudDistants =0;

    }
    public void suusata() {
        if ((int) (Math.random()* 2500) == 0) {  //väike shans et juhtub nii, et suusataja katkestab
            dopinguKordaja = 0;
            System.out.println("Võistleja nr " + stardiNr + " katkestas");
        }
        labitudDistants += kiirus/(3600);

    }
    public String toString () {
        int dist = (int) (labitudDistants * 1000); //castime intideks
        return stardiNr + ": " + dist; //kui tahame pärast näga arraylisti kus on kõik suusatajad, kutsun välja
    }


    public boolean kasOnLopetanud(int koguDistants) {
        return labitudDistants >= koguDistants;
    }
}

//kodune ülesanne
//