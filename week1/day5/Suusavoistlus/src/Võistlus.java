import java.util.ArrayList;

public class Võistlus {  //Klass nimega Võistlus/ ArrayList tüüpi klass
    ArrayList<Suusataja>voistlejad; //Loome ArrayListi kus sees on "Suusataja" objekt. muutuja konstructori sees
    int koguDistants;       //uus int muutuja

    public Võistlus () { //Konstruktor, sama nimega mis klass! Konstruktori roll on luua objekt. Konstruktor kutsutakse välja new operaatori abil.
        System.out.println("Võistlus start!");

        voistlejad = new ArrayList(); //uus ArrayList nimega võistlejad
        koguDistants = 20;     //määrame võistluse distantsiks 20 km.

        for (int i = 0; i < 50; i++) { //Määrame, et võistlejaid on 50 tk.

            voistlejad.add(new Suusataja(i)); //lisame ArrayListi Suusatajad, ehk i
        }
        aeg();
    }
    public void aeg() { //aeg ehk tsükkel mille jooksul iga suusataja läbib mingi distantsi
        for (Suusataja s: voistlejad){ //foreach tüüp tsükkel, võtame arraylisti, kus võtame läbi iga suusataja. S on üks suusataja
            s.suusata();    //anname igale suusatajale (s) käsu suusata
           boolean lopetanud = s.kasOnLopetanud(koguDistants);
           if (lopetanud) { //Kui keegi on läbinud distantsi 20 km
               System.out.println("Võitja on " + s); //võtab võitja, kes esimesena jõuab lõppu
               return; //peale võitja selgitamist mäüng läheb kinni
           }
        }
        System.out.println(voistlejad);
        try {                                       //ohtlik meetod, peame trycatchima
            Thread.sleep(10);                   //Thread - 1 mõttejoon/duum(arvutil 8 duuma ehk mõttejada korraga) - panne mõtlemine magama üheks sekundiks aeg, ehk 1 sekund
        } catch (InterruptedException e) {              //errror handling, kui jookseb errori tõttu kokku, siis see pääastab ja jookseb edasi
            e.printStackTrace();
        }
        aeg();                                                      //Kutsume välja aja, ehk kõik suusatajad liiguvad edasi oma distantsi iga sekund

    }       //tsükkel mis ütleb igale suusatajale "Suusata!


}
