public class Main {

    public static void main(String[] args) { //Põhiklass

        /* KODUNE ÜL:
        1. dopingu kordaja (mõni on lihtsalt parem kui teine) - 2 varianti - üks võimalus on märkida dopingu
        tegija võistluse klassis mis tähendaks et peab tegema suusatajale teise sisendparameetri veel analoogselt
        järjekorra numbrile
        teine võimalus on võtta suusataja klasis et nt. kui järjekorra number on 5
        siis lisaks korruta kiirus veel mingi dopingukordajaga


        2. Päris 0 kiirusega ei tohiks keegi sõita
        3. Keegi sai viga ja katkestab (randomilt)
        4. Joonista võistlus loetavalt välja (mission impossible)
         */

        System.out.println("Võistlus alga!");
        new Võistlus(); //loome Võistluse põhiklassi ehk Võistlus objekti, kus sees genereerime 50 suusatajat, millest võistlus koosneb
    }
}
