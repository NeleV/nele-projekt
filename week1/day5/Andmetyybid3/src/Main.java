import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hi!");

        //ÜL:
        // 1. Loo kolm muutujat numbritega
        // 2. Moodusta lause nende muutujatega
        // 3. Prindi see lause välja
        int aasta = 88;
        int kuu = 9;
        int paev = 16;
        String lause = "Ma sündisin aastal " + aasta + ". Kuu oli " + kuu + " ja kuupaev " + paev +".";
        System.out.println(lause);

        //%d on täisarv
        //%f on komakohaga
        //%s on string

      //  String parem = String.format("Mina sündisin aastal %d. ", aasta + "Kuu oli %d ja kuupäev %d.", aasta, kuu, paev);// string.format - iga % märk, on samas järjekorras mis muutujad.

        // System.out.println(parem);

        //ÜL: Loo HashMap järgneva infoga:
        //1. Loe kokku mitu lampi on klassis ja pane see info HashMappi!
        //2. Leo kokku mitu akent on klassis ja pane HashMappi
        //3. Loe kokku mitu inimest on klassis ja pane HashMappi

        HashMap klassiAsjad = new HashMap(); //Loome uue hashmapi
        klassiAsjad.put("aknad", 5);  // Paneme sisse esimese ülesande vastuse
        klassiAsjad.put("lambid", 8);
        klassiAsjad.put("inimesi", 19);
        klassiAsjad.put("lambid", 90);

        //ei saa teha kahte sama võtmega väärtust, kui lisad veel ühe sama nimega, ta kirjutab üle

        System.out.println(klassiAsjad);

        //Mõtle välja 3 praktilist kasutust HashMapile

        // 1.inventuur - külmpapis on niipalju õlut alles
          //  2. Rühmas on (... last)
        // 3. Sahtlis on ( 3 küünalt)
        //HashMap on pmst nagu exeli tabel ja kaks tulpa

        // ÜL: prindi välja kui palju on inimesi klassis, kasutades juba loodud hashmappi.
        int inimesi = (int) klassiAsjad.get("inimesi"); //kutsun välja klassiasjad, vajutan. ja vaatan mida saab valida.

        System.out.println(inimesi);

        //ÜL: Lisa samasse HashMappi juurde mitu tasapinda on klassis, aga number enne ja siis String
        //nt. (10: "tasapinnad")

        klassiAsjad.put(10, "tasapinnad"); //kui on numbrid ees, siis on ta nagu array
        System.out.println(klassiAsjad);

        //ÜL: Loo uus HashMap, kuhu saab sisestada AINULT String: double paare. Sisesta sinna midagi

        HashMap<String, Double> paarid = new HashMap<>(); //Loon uue Hashmapi
        paarid.put("Paarid", 10.2);
        paarid.put("needKa", 17.8);
        paarid.put("jaSee", 9.3);

        System.out.println(paarid);

        //HashMappi kasuta siis kui vaja on!

        //IF-i keerulisem variant
        //ÜL: switch

        int rongiNr = 50;  //Rong nr 50, raudtee läheb 2-ks. IF-iga vali kuhu läheb //Switch on see, kui tee läheb 10-neks
        String suund = null; //hetkel veel ei tea mis suund on
        switch (rongiNr) {
            case 50: suund = "Pärnu"; break; //
            case 55: suund = "Haapsalu"; break; //Erinevad suunad valikus
            case 10: suund = "Vormsi"; break;
        }
        System.out.println(suund);

        //Switch uuesti!
        // ÜL: Õpilane saab töös punkte 0-100
        //1. kui punkte on alla 50, kukub ta töö läbi
        //2. vastasel juhul on hinne täisarv, punktid/20 (jagatud)
        //100 punkti => 5
        //80 punkti => 4
        //67 punkti => 3
        //50 punkti => 2

        int punkte = 99;
        if (punkte > 100 || punkte < 0) { //juhul kui punkte on rohkem kui vahemikus, throw error
            throw new Error();
        }
        switch ((int) Math.round (punkte / 20.0)) {
            case 5:
                System.out.println("suurepärane");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 2:
                System.out.println("ee..õppisid ka?");
                break;
            default: //
                System.out.println("Kukkusid läbi");
                break;
        }

            /* if( suund = 50){
                System.out.println("Pärnu"); break;
            } else if (suund = 55){
                System.out.println("Haapsalu"); break;
            } else {
                System.out.println("Haapsalu");
            }*/

            // FOREACH
        int[] mingidNumbrid = new int[] {8, 4, 6, 345, 8978, 12345235}; //Intide massiv
        for (int i = 0; i < mingidNumbrid.length; i++) {
            System.out.println(mingidNumbrid[i]);
        }
        System.out.println("---------------------");
            //lihtsam variant
        for(int nr : mingidNumbrid ) {  //paremal pool nimekiri, pannakse väärtusi nr alla, korda mööda. Käib ükshaaval array läbi!
            System.out.println(nr);
        }






    }
}
