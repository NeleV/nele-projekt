import nadalapaevad.Laupäev;
import nadalapaevad.Pyhapaev;
import nadalapaevad.Reede;

public class Main {

    public static void main(String[] args) {

        System.out.println("Hello, World!");
        if ((false && true) || true) {  //boolean
            System.out.println("Tõene");
        } else {
            System.out.println("Väär");
        }

        Reede.koju();
        //reede on klass, koju on meetod.
        //klassi ja meetodi loomise shotcut:
        // klikka peale ja Alt+Enter

        Laupäev.peole();

        Pyhapaev.hommik();


        Pyhapaev paev = new Pyhapaev();
        paev.maga();//kutsume meetodi välja, paneme uue nime
        //paev.uni(); //ei saa kätte, kuna uni on private meetod

        paev.hommik();

    }
}

