console.log("hommik!")	

var sisend = 7
var tulemus 

//kui on vähem kui 7, korruta 2-ga, kui suurem kui 7, jaga 2-ga//
//kui on täpselt 7, jäta samaks

if (sisend == 7) {
	tulemus = sisend
} else if(sisend > 7) {
	tulemus = sisend * 2 
} else {
	tulemus = sisend / 2 
}

console.log("Tulemus on: " + tulemus)

/*
kui sõned on võrdsed, siis prindi üks, kui erinevad siis liida kokku ja prindi.
*/

var str1 = "banaan"
var str2 = "apelsin"

if (str1 == str2) {
	console.log("banaan")
} else {
	console.log(str1 + " " + str2)
}

/*
MEil on linnade nimekiri, aga ilma sõnata "Linn". Need palun llisada
*/

var linnad = ["Tallinn", "Tartu", "Valga"] // Defineerime nimekirja
var uuedLinnad = []	//defineerime uue listi kuhu tulevad tulemused
// niikaua kui listi pikkus on suurem kui 0, käivita funktsiooni
 
 while (linnad.length > 0) {  //kui linnasid on listis
	var linn = linnad.pop()  	//võta välja viimane
	var uusLinn = linn + " linn" //lisa +linn otsa
	uuedLinnad.push(uusLinn) //tulemus salvesta uude listi
}

console.log(uuedLinnad)


/* Eralda poiste ja tüdrukute nimed
*/

var nimed = ["Margarita", "Mara", "Martin", "Kalev"]
var poisteNimed = []
var tydrukuteNimed = []

while (nimed.length > 0) {
	
	
	//"a" lõpuga on tüdruku nimi
	//Kui poisi nimi, siis poiste listi
	//Kui tüdruku siis tüdrukute listi

	var nimi = nimed.pop()
	if (nimi.endsWith("a")) {	
	tydrukuteNimed.push(nimi)
} else {
	poisteNimed.push(nimi)
}
}
console.log(poisteNimed, tydrukuteNimed)

//ctrl + klikk saad mitu kursorit
//crtl + enter tekitad uue rea
//console.log("muutuja", muutuja) prindib 


/* FUNKTSIOONID */

/*
	Kirjuta algoritm, mis suudab ükskõik mis naise/mehe nime eristada.
*/

var eristaja = function(nimi) {
	if (nimi.endsWith("a")) {	
		return "tüdruk"
} else {
	return "poiss"
}
}
var praeguneNimi = "Peeter"

var kumb = eristaja(praeguneNimi)
console.log(kumb)


/*
Loo funktsioon, mis tagastab vastuse küsimusele kas tegu on numbriga?
!isNaN(4) ehk is Not a Number. Hüüumärk pöörab true/false vastupidi
*/

var kasOnNumber = function(number) {
	if (!isNaN(number)) { 
		return true
} else {
	return false
}
}

console.log(kasOnNumber("mingi sõne") )
console.log(kasOnNumber(23563) )
console.log(kasOnNumber(6.876) )
console.log(kasOnNumber(4))
console.log(kasOnNumber(null) )
console.log(kasOnNumber([1, 4, 5, 6]) )



/* 
Kirjuta funktisioon, mis võtab vastu kaks numbrit ja tagastab nende summa
*/
/*
	var num1 = 8
	var num2 = 17
	vastus = num1+num2
	*/

var summa = function (a, b) {
	return a + b	
} 
console.log(summa(4, 5))
console.log(summa(8, 72))
console.log(summa(89024, 6794))


/*


console.log("------------")

var inimesed = {
	"kaarel": 34,
	"Margarita": 10,
	"Suksu": [3, 4, 5]
	"Krister": {
		vanus 30,
		sugu: true
	}
}
//võtab võtmest kaarel väärtuse välja (34)
console.log(inimesed["kaarel"])
console.log(inimesed.kaarel)

console.log(inimesed.Krister.sugu)
console.log(inimesed.Suksu[1])
*/

