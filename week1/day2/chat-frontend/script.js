console.log("T66tan!")

/* 1. alla laadida API'st tekst.
2. 
*/

var refreshMessages = async function() {
	//Selleks, et ma tean, et funktsioon läks käima
	console.log("refreshMessages l2ks k2ima")
	// API aadress on string, salvestan lihtsalt muutujasse
	var APIurl = "http://138.197.191.73:8080/chat/general"
	//fetch teeb päringu serverisse (meie defineeritud aadress)
	var request = await fetch(APIurl)
	//json() käsk vormib meile data mugavaks JSONiks
	var json = await request.json()

	//queryselector on käsk, # on viide ID-le ja jutt-jutt, .innerHTML määrab uue osa teksti sisse
	//))	document.querySelector('#jutt').innerHTML = JSON.stringify(json)

	//Kuva serverist saadud info HTMLis (ehk lehel)
	document.querySelector('#jutt').innerHTML = ""
	var sonumid = json.messages
	while (sonumid.length > 0) { // kuniks sõnumeid on 
		// wõi ainult while (json.messages.length > 0) {
			var sonum = sonumid.shift()
			console.log(sonum)
			// lisa HTMLi #jutt sisse sonum.message"
		document.querySelector('#jutt').innerHTML += "<p>" + sonum.user + ": " + sonum.message + "</p>"
	}
	//scrolli koige alla
	window.scrollTo(0,document.body.scrollHeight);
} //Uuenda sõnumeid iga sekund
setInterval(refreshMessages,10000)


document.querySelector('form').onsubmit = function(event) {
	event.preventDefault() //viivituse tekitamiseks
	//console.log("Submit kaivitus")

	//korjame kokku formist info
	var username = document.querySelector('#username').value
	var message = document.querySelector('#message').value
	document.querySelector('#message').value = "" //tee input tühjaks
	console.log(username, message)
//POST päring postitab uue andmetüki serverisse
	var APIurl = "http://138.197.191.73:8080/chat/general/new-message" //see on serveri poolt antud URL
	fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({user: username, message: message}),
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}

	})
} 
