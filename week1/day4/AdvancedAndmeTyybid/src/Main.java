import java.util.ArrayList;
import java.util.Arrays;
import java.lang.Math;

public class Main {

    public static void main(String[] args) {
        //Keskseks teemaks on array ehk massiiv
        /*Intide massiiv [] nimi = uus Int-ide massiiv [piikkus];
        Primitiividest koosneb
        massiiv on nimekiri */
        int[] massiiv = new int[6];
        //ArrayList list = uus
        //objekt, arraylist on veniv!
        ArrayList list = new ArrayList();
        //ÜL: prindi välja massiiv
        String massiivStr = Arrays.toString(massiiv);
        System.out.println(massiivStr);

        //ÜL: Määra 3 positsioonil numbri massiivis nr. 5-ks
        //taham nassiivi muuta (kolmandat kohta)
        massiiv[2] = 5;
        //arrays.tostring et saada kogu nimekiri!
        System.out.println(Arrays.toString(massiiv));

        //ÜL: Prindi välja kuues element massiivist( aga enne määra talle väärtus)
        massiiv[5] = 7;
        System.out.println(massiiv[5]);

        //ÜL: Prindi välja viimane element, ükskõik kui pikk massiv ka ei oleks.
        //viimane element: massiiv.length-1
        //array piikkus on alati 1 rohkem, seega -1!!!
        int viimane = massiiv[massiiv.length - 1];
        System.out.println("viimane " + viimane);

        //ÜL: Loo uus massiiv, kus on kõik 8 numbrit kohe alguses määratud
        int[] massiiv8 = new int[]{1, 2, 3, 4, 55, 6, 7, 99};
        System.out.println(Arrays.toString(massiiv8));

        //ÜL: Prindi välja ükshaaval kõik väärtused massiiv8-st

        //Määrame mitmes meie index praegu on
        int index = 0;
        //Teeme while tsükkil, mis kestab niikaua kuni indeks on väikesem kui massiv8.length ja tema sisus ütleme, et prindime välja selle massiiv8, indeksil ja index++
        while (index < massiiv8.length) {
            System.out.println(massiiv8[index]);
            index++;
        }
        //ÜL: Teeme selle sama tsükli kiiremini kasutades for tsükklit
        for (int i = 0; i < massiiv8.length; i++) {
            System.out.println(massiiv8[i]);
        }

        //ÜL: Loo Stringide massiivid, mis on alguses tühi
        // 2. siis lisad ka veel keskele mingi sõne
        int[] nimekiri = new int[3];
        ArrayList rida = new ArrayList();
        String nimekiriStr = Arrays.toString(nimekiri);
        System.out.println(nimekiriStr);
        nimekiri[1] = 8;
        System.out.println(Arrays.toString(nimekiri));

        //Tegelik vastus!!

        String[] stringid = new String[3];
        stringid[1] = "Kalevipoeg";
        System.out.println(Arrays.toString(stringid));

        //ÜL: 1. Loo massiiv, kus on 100 kohta
        // 2. Sisesta sellesse massiivi loetelu numbreid 0.... 99
        // 3. Prindi välja see mega massiiv

        int[] megaMass = new int[100];
        for (int i = 0; i < megaMass.length; i++) {
            // System.out.println(i);
            megaMass[i] = i;
        }
        System.out.println(Arrays.toString(megaMass));


        int mituPaarisArvuOn = 0; //algväärtus on 0, millelel lisatakse iga kord kui paarisarv, ehk 0 on 0, siis 0+1, jne
        for (int i = 0; i < megaMass.length; i++) {
            if (i % 2 == 0) {
                mituPaarisArvuOn = mituPaarisArvuOn + 1;

            }
        }

        System.out.println("mituPaarisArvuOn " + mituPaarisArvuOn);


        //ÜL:
        //1. kasuta megaMass massiivi, kus on numbrite jada
        //2. loe mitu paaris arvu on?
        //3. prindi tulemus välja
        //kasutada tuleb nii tsüklit kui ka if lauset
        // Kui jagad kaks arvu operaatoriga % siis see tagastab jäägi
        // x % 2 tähendab, et 0 puhul on paarisarv ja 1 puhul ei ole

        // Algoritm (tegevus) on järgmine:
        //1. võtan esimese numbri massiivist. See on 0
        //2. Ma küsin kas see number on paaris või mitte?
        //3. Kuna on paaris, siis liidan ühe loendajale otsa.

        //Algoritm:
        //1. võtan esimese numbri massivist
        //2. kui number on paaris?
        //3. Siis lisan ühe loendajale otsa

        /* public static void main(String[] args) {
        //Keskseks teemaks on array ehk massiiv
        /*Intide massiiv [] nimi = uus Int-ide massiiv [piikkus];
        Primitiividest koosneb
        massiiv on nimekiri
        int[] massiiv = new int[6];
        //ArrayList list = uus
        //objekt, arraylist on veniv!
        ArrayList list = new ArrayList();
        //ÜL: prindi välja massiiv
        String massiivStr = Arrays.toString(massiiv);
        System.out.println(massiivStr);
        */

        ArrayList list2 = new ArrayList();
        list2.add(4);
        list2.add(8);
        list2.add(954879);
        list2.add("Krister");
        list2.add("Peeter");
        list2.add(4);

        //ÜL: Küsi viimasest listist välja kolmas element ja pringi välja

        System.out.println(list2.get(2));

        //ÜL: Prindi kogu list välja!

        System.out.println(list2);

        //ÜL: Prindi iga elemenet ükshaaval välja


        for (int i = 0; i < list2.size(); i++) {
            System.out.println(list2.get(i));
        }

        //ÜL: Loo uus ArrayList, kus on nt 543 numbrit
        //1.1 Nubrid peavad olema suvalised ehk Math.random() vahemikus 0-10
        //2. siis korruta iga number viiega
        //3. salvesta see uus number samale positsioonile


        int[] arrayInput = new int[543];
        ArrayList list3 = new ArrayList(); // uus arrayList, suurus on määramata
        for (int y = 0; y < 543; y++) { // loome 543 numbrit, mida lisada arrayListi
            list3.add((int) (Math.random() * 10 + 1)); //lisab 543 random numbrit list
        }
        System.out.println(list3); //Prindi list kontrolliks

        for (int i = 0; i < 543; i++) {
            int kordaViis = (int) list3.get(i) * 5; //Numbrite korrutamine viiega, .get võtab nad arrayListist välja
            list3.set((i), kordaViis); // viiekordsete nr-te lisamine tagasi listi ja eelmistega asendamine - .set
        }
        System.out.println(list3);


            /* for (int Y = 0; y < arrayInput.length; y ++) {
                 arrayInput = int [] arrayInput[y]*5;
                System.out.println(arrayInput);*/

        //Teine variant:

        ArrayList<Integer> massiiv543 = new ArrayList(); // Loome uue listi //Kui panna ArrayList <Integer> ütleb, et selles listis võivad olla ainult integerid ja .floor võime ära võtta
        for (int i = 0; i < 543; i++) {  //teeme array 543 koha pikkuseks - see rida puudutab ainult arrayListi indekse
            int nr = (int)(Math.random() * 11); //See rida on arrayListi väärtuste jaoks, ehk kastide sisu! Math võtab tagant komakoha ära. Korrutad 11-nega kuna muidu ei saa kunagi 10-t
            // System.out.println(nr); - kontrolliks
            massiiv543.add(nr); //lisame .add käsuga numbri (nr) arrayListi massiiv543, ehk täidame listti 543 korda!
        }
         System.out.println("Algne massiiv: " + massiiv543); // kontrolliks, kas toimib
        // hakkame massiiv543 sisu muutma, kasutame for tsükklit

        for (int i = 0; i < massiiv543.size(); i++) { //Teeme tsükkli läbi arvuga, mis on võrdeline massiivi pikkusega, ehk kasutame massiiv543.size()
            //double nr = massiiv543.get(i) - kui oleks double swellisel kujul. Eelmine double oli lokaalne bloki suhtes
            int nr = (int) massiiv543.get(i); //Teen uue muutuja, mille saan kätte kasutades massiivi indeksit (i)
            int muudetudNr = nr * 5; // teeme uue muutuja ja korrutame numbreid 5-ga, salvestame need muutujad nimegha muudetudNr

            massiiv543.set((i), muudetudNr);
            ;   //set-iga paned uued muutujad tahasi ArrayListi(ütleb ka sulgudes, et tahab indeksi ja elementi, mis sel juhul on muudetudNr
        }
        System.out.println("Muudetud massiiv: " + massiiv543);
    }
}









/*System.out.println(i);
        System.out.println(Arrays.toString(pikk)); */

//ÜL: Loo ArrayList nja sisesta sinna 3 numbrit ja kaks Stringi



//kirjuta kommentaaridesse miks sa midagi teed, mitte kuidas sa teed