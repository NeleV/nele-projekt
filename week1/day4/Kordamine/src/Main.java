public class Main {
    //main meetod
    public static void main(String[] args) {
        //määrame erinevad väärtused, mida arvutada
        double num1 = 5.12;
        double num2 = 7.0;
        double num3 = 12.2;
        System.out.println(Kodutöö.ruut (num1));
        System.out.println(Kodutöö.ruut (num2));
        System.out.println(Kodutöö.ruut (num3));

        int tulemus = Kodutöö.astendaja(2, 7);
        System.out.println(tulemus);

        //.getVanus()- sulud selleks, et anda sisend

        Kodutöö.getVanus();
        System.out.println(Kodutöö.getVanus());

        //uue meetodi loomine - = new ja klass
        Kodutöö kt = new Kodutöö();
        System.out.println(kt.vanus);
        kt.korrutaja(2);

        System.out.println(kt.vanus);
        //staatiline ja mitte staatiline on klass või objekt
        //static on klass
        //objekt on dünaamiline, saavad muutuda väärtused
        //objekti meetodi saad kasutada

    }
}
