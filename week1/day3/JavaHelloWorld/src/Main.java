import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        System.out.println("sout"); //sout on kiirkäsk
        ; // iga rea lõpus peab olema semikoolon!!

        //primitiivsed muutujuad:
        int number = 5; //Täisarv, interger Üksik number (4 byte suuruses)
        double n2 = 5.1; //komakohaga number
        float n3 = 5.9f; // ?
        byte bait = 5; //255 väärtust /-128  kuni 127 (8 biti = 1 byte)
        // long suuremKuiInt =

        // BigDecimal() //midagi midagi

        //objekt tüüpi muutuja
        String nimi = "Krister";
        nimi.length();

        char algusTaht = 'K';

        if (number == 5) { //if syntax on täpselt sama mis JavaScriptis
        } else {
        }
        String liitumine = "tere " + 5;
        System.out.println(liitumine);

        int liitmine = (int) (number + n2); //cast'ing ühest tüübist teise
        System.out.println(liitmine);
        System.out.println(Math.round(10.7)); //ümardamiseks eraldi meetod

        int parinsNumber = Integer.parseInt("4");
        double parisNumber2 = Double.parseDouble("4.1");


        String puuvili1 = "Banaan";
        String puuvili2 = "Banaan";
        if (puuvili1 == puuvili2) {
            System.out.println("PUUVILJAD ON VÕRDSED");
        } else {
            System.out.println("EI OLE VÕRDSED");
        }
        Koer.auh(); //kutsume klassi Koer välja
        Koer.lausu();
        Kass.mjau();
        Koduloom.lausu();
    }
    public static int summa(int a, int b) {
        return a + b;
    }
}




